import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RepositoriesFilter } from './repositories-filter';

@Component({
  selector: 'app-repositories-filter',
  templateUrl: './repositories-filter.component.html'
})
export class RepositoriesFilterComponent implements OnInit {
  @Output()
  search = new EventEmitter<RepositoriesFilter>();

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required]
    });
  }

  submit() {
    const filterValue: RepositoriesFilter = {
      username: this.form.value.username
    };

    this.search.emit(filterValue);
    this.form.reset();
  }
}
