import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RepositoriesFilterComponent } from '../repositories-filter.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { RepositoriesFilterComponentObject } from './repositories-filter.component-object';
import { take } from 'rxjs/operators';

describe('RepositoriesFilterComponent', () => {
  let component: RepositoriesFilterComponent;
  let fixture: ComponentFixture<RepositoriesFilterComponent>;
  let componentObject: RepositoriesFilterComponentObject;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RepositoriesFilterComponent],
      providers: [FormBuilder],
      imports: [ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoriesFilterComponent);
    component = fixture.componentInstance;
    componentObject = new RepositoriesFilterComponentObject(fixture);

    fixture.detectChanges();
  });

  it('should disable the submit button on load', () => {
    expect(componentObject.submitButton.disabled).toBeTrue();
  });

  it('should enable the submit button when a username is entered', () => {
    componentObject.username.value = 'some name';
    componentObject.username.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(componentObject.submitButton.disabled).toBeFalse();
  });

  it('should disable the submit button when a username is empty', () => {
    componentObject.username.value = 'some name';
    componentObject.username.value = '';
    componentObject.username.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(componentObject.submitButton.disabled).toBeTrue();
  });

  it('should display an error message when a username is empty', () => {
    componentObject.username.value = 'some name';
    componentObject.username.value = '';
    componentObject.username.dispatchEvent(new Event('input'));
    componentObject.username.dispatchEvent(new Event('blur'));
    fixture.detectChanges();

    expect(componentObject.errorMessage).not.toBeNull();
  });

  it('should emit form values and reset the form when the submit button is clicked', () => {
    componentObject.username.value = 'some name';
    componentObject.username.dispatchEvent(new Event('input'));

    let filterEmitted = null;
    component.search
      .pipe(
        take(1)
      )
      .subscribe(filterValue => {
        filterEmitted = filterValue;
      });

    componentObject.clickSubmitButton();
    fixture.detectChanges();

    expect(filterEmitted).not.toBeNull();
    expect(filterEmitted.username).toEqual('some name');
    expect(componentObject.username.value).toBeFalsy();
  });
});
