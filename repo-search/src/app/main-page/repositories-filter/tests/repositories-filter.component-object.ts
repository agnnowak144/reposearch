import { RepositoriesFilterComponent } from '../repositories-filter.component';
import { By } from '@angular/platform-browser';
import { AbstractComponentObject } from '../../shared/tests/abstract.component-object';

export class RepositoriesFilterComponentObject extends AbstractComponentObject<RepositoriesFilterComponent> {
  get username(): HTMLInputElement {
    return this.getElementByTestId('username');
  }

  get submitButton(): HTMLInputElement {
    return this.getElementByTestId('submitButton');
  }

  get errorMessage(): HTMLInputElement {
    return this.getElementByTestId('usernameErrorMessage');
  }

  clickSubmitButton() {
    this.fixture.debugElement.query(By.css(`[${this.testAttributeName}=submitButton]`)).triggerEventHandler('click', null);
  }
}
