import { Branch } from './branch';

export interface Repository {
  name: string;
  owner: string;
  branches: Branch[];
}
