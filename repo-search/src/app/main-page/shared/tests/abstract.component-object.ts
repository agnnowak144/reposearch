import { ComponentFixture } from '@angular/core/testing';

export abstract class AbstractComponentObject<TComponent> {
  protected readonly testAttributeName = 'data-test-id';

  constructor(protected fixture: ComponentFixture<TComponent>) {
  }

  protected getElementByTestId(id: string): any {
    return this.fixture.nativeElement.querySelector(`[${this.testAttributeName}=${id}]`);
  }
}
