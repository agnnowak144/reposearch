import { Component, Input } from '@angular/core';
import { Repository } from '../shared/repository';

@Component({
  selector: 'app-repositories-list',
  templateUrl: './repositories-list.component.html'
})
export class RepositoriesListComponent {
  @Input()
  userName: string;

  @Input()
  repositories: Repository[];

  get hasRepositories(): boolean {
    return !!this.repositories && this.repositories.length > 0;
  }
}
