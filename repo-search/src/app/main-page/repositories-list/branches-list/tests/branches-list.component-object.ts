import { BranchesListComponent } from '../branches-list.component';
import { AbstractComponentObject } from '../../../shared/tests/abstract.component-object';

export class BranchesListComponentObject extends AbstractComponentObject<BranchesListComponent> {
  get branches(): HTMLElement[] {
    return this.fixture.nativeElement.querySelectorAll(`[${this.testAttributeName}=branchData]`)
  }

  get header() {
    return this.getElementByTestId('header');
  }

  get noBranchesMessage(): HTMLElement {
    return this.getElementByTestId('noBranchesMessage');
  }

  getBranchData(branchIndex: number) {
    return this.branches[branchIndex];
  }
}
