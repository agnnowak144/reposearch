import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BranchesListComponent } from '../branches-list.component';
import { BranchesListComponentObject } from './branches-list.component-object';

describe('BranchesListComponent', () => {
  let component: BranchesListComponent;
  let fixture: ComponentFixture<BranchesListComponent>;
  let componentObject: BranchesListComponentObject;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BranchesListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesListComponent);
    component = fixture.componentInstance;
    componentObject = new BranchesListComponentObject(fixture);
    fixture.detectChanges();
  });

  it('should display only an empty list message when there are no branches', () => {
    component.branches = [];
    fixture.detectChanges();

    expect(componentObject.noBranchesMessage).not.toBeNull();
    expect(componentObject.branches.length).toEqual(0);
  });

  it('should display branches when there are any', () => {
    component.branches = [
      {
        name: 'branch 1',
        lastCommitSha: 'sha 1'
      },
      {
        name: 'branch 2',
        lastCommitSha: 'sha 2'
      }
    ];
    fixture.detectChanges();

    expect(componentObject.noBranchesMessage).toBeNull();
    expect(componentObject.branches.length).toEqual(2);
    expect(componentObject.header.textContent).toContain('Branches: 2');
    expect(componentObject.getBranchData(0).textContent).toContain('1) branch 1 (last commit: sha 1)');
  });
});
