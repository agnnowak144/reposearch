import { Component, Input } from '@angular/core';
import { Branch } from '../../shared/branch';

@Component({
  selector: 'app-branches-list',
  templateUrl: './branches-list.component.html'
})
export class BranchesListComponent {
  @Input()
  branches: Branch[];

  get hasBranches(): boolean {
    return !!this.branches && this.branches.length > 0;
  }
}
