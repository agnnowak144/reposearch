import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepositoriesListComponent } from '../repositories-list.component';
import { RepositoriesListComponentObject } from './repositories-list.component-object';

describe('RepositoriesListComponent', () => {
  let component: RepositoriesListComponent;
  let fixture: ComponentFixture<RepositoriesListComponent>;
  let componentObject: RepositoriesListComponentObject;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RepositoriesListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoriesListComponent);
    component = fixture.componentInstance;
    componentObject = new RepositoriesListComponentObject(fixture);
    fixture.detectChanges();
  });

  it('should display only an empty list message when there are no repositories', () => {
    component.repositories = [];
    fixture.detectChanges();

    expect(componentObject.noRepositoriesMessage).not.toBeNull();
    expect(componentObject.repositories.length).toEqual(0);
  });

  it('should display repositories when there are any', () => {
    component.repositories = [
      {
        name: 'repo 1',
        owner: 'owner 1',
        branches: []
      },
      {
        name: 'repo 2',
        owner: 'owner 2',
        branches: []
      }
    ];
    fixture.detectChanges();

    expect(componentObject.noRepositoriesMessage).toBeNull();
    expect(componentObject.repositories.length).toEqual(2);
    expect(componentObject.getRepositoryHeader(0).textContent).toContain('1. repo 1 (owner: owner 1)');
  });
});
