import { RepositoriesListComponent } from '../repositories-list.component';
import { AbstractComponentObject } from '../../shared/tests/abstract.component-object';

export class RepositoriesListComponentObject extends AbstractComponentObject<RepositoriesListComponent> {
  get repositories(): HTMLElement[] {
    return this.fixture.nativeElement.querySelectorAll(`[${this.testAttributeName}=repositoryData]`)
  }

  get noRepositoriesMessage(): HTMLElement {
    return this.getElementByTestId('noRepositoriesMessage');
  }

  getRepositoryHeader(repositoryIndex: number): HTMLElement {
    return this.repositories[repositoryIndex].querySelector(`[${this.testAttributeName}=repositoryHeader]`);
  }
}
