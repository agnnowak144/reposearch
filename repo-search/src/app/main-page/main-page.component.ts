import { Component } from '@angular/core';
import { RepositoriesFilter } from './repositories-filter/repositories-filter';
import { Repository } from './shared/repository';
import { MainPageService } from './main-page.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html'
})
export class MainPageComponent {
  searchedUsername: string;
  isLoadingRepositories = false;
  repositories: Repository[];
  errorMessage: string;

  constructor(private service: MainPageService) {
  }

  search(filter: RepositoriesFilter) {
    this.searchedUsername = filter.username;
    this.isLoadingRepositories = true;
    this.errorMessage = null;

    this.service.getRepositoriesForUser(this.searchedUsername)
      .pipe(
        take(1)
      )
      .subscribe(
        value => {
          this.repositories = value;
          this.isLoadingRepositories = false;
        },
        error => {
          if (error.status && error.status === 404) {
            this.errorMessage = `The user ${this.searchedUsername} doesn't exist.`;
          } else {
            this.errorMessage = `An error occurred while loading repositories for the user ${this.searchedUsername}.`;
            console.log(error);
          }
          this.repositories = [];
          this.isLoadingRepositories = false;
        });
  }
}
