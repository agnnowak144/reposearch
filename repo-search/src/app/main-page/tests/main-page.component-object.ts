import { AbstractComponentObject } from '../shared/tests/abstract.component-object';
import { MainPageComponent } from '../main-page.component';

export class MainPageComponentObject extends AbstractComponentObject<MainPageComponent> {
  get spinner() {
    return this.getElementByTestId('loadingSpinner');
  }

  get repositoriesList() {
    return this.getElementByTestId('repositoriesList');
  }

  get errorMessage() {
    return this.getElementByTestId('errorMessage');
  }
}
