import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { MainPageComponent } from '../main-page.component';
import { MainPageComponentObject } from './main-page.component-object';
import { MainPageService } from '../main-page.service';
import { Subject } from 'rxjs';
import { Repository } from '../shared/repository';

describe('MainPageComponent', () => {
  let component: MainPageComponent;
  let fixture: ComponentFixture<MainPageComponent>;
  let componentObject: MainPageComponentObject;

  it('should display a spinner while loading repositories for a given user', fakeAsync(() => {
    const serviceSpy = jasmine.createSpyObj('MainPageService', ['getRepositoriesForUser']);
    const mockRepositoriesObservable = new Subject<Repository[]>();
    serviceSpy.getRepositoriesForUser.and.returnValue(mockRepositoriesObservable);

    TestBed.configureTestingModule({
      declarations: [MainPageComponent],
      providers: [
        { provide: MainPageService, useValue: serviceSpy }
      ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(MainPageComponent);
        component = fixture.componentInstance;
        componentObject = new MainPageComponentObject(fixture);
        fixture.detectChanges();

        component.search({
          username: 'user'
        });
        fixture.detectChanges();

        expect(componentObject.spinner).not.toBeNull();
        expect(componentObject.repositoriesList).toBeNull();

        mockRepositoriesObservable.next([]);
        fixture.detectChanges();

        expect(componentObject.spinner).toBeNull();
        expect(componentObject.repositoriesList).not.toBeNull();
      });
  }));

  it('should display an error message when an error occurs while loading repositories', fakeAsync(() => {
    const serviceSpy = jasmine.createSpyObj('MainPageService', ['getRepositoriesForUser']);
    const mockRepositoriesObservable = new Subject<Repository[]>();
    serviceSpy.getRepositoriesForUser.and.returnValue(mockRepositoriesObservable);

    TestBed.configureTestingModule({
      declarations: [MainPageComponent],
      providers: [
        { provide: MainPageService, useValue: serviceSpy }
      ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(MainPageComponent);
        component = fixture.componentInstance;
        componentObject = new MainPageComponentObject(fixture);
        fixture.detectChanges();

        component.search({
          username: 'user'
        });

        mockRepositoriesObservable.error('error message');
        fixture.detectChanges();

        expect(componentObject.errorMessage).not.toBeNull();
        expect(componentObject.spinner).toBeNull();
        expect(componentObject.repositoriesList).toBeNull();
      });
  }));

  it('should hide an error message when data is loaded after an error', fakeAsync(() => {
    const serviceSpy = jasmine.createSpyObj('MainPageService', ['getRepositoriesForUser']);
    const mockRepositoriesObservables = [
      new Subject<Repository[]>(),
      new Subject<Repository[]>()
    ];

    let currentRepositoriesObservableIndex = 0;
    serviceSpy.getRepositoriesForUser.and.callFake(() => {
      const result = mockRepositoriesObservables[currentRepositoriesObservableIndex];
      currentRepositoriesObservableIndex++;
      return result;
    });

    TestBed.configureTestingModule({
      declarations: [MainPageComponent],
      providers: [
        { provide: MainPageService, useValue: serviceSpy }
      ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(MainPageComponent);
        component = fixture.componentInstance;
        componentObject = new MainPageComponentObject(fixture);
        fixture.detectChanges();

        let currentRepositoriesObservable = mockRepositoriesObservables[currentRepositoriesObservableIndex];
        component.search({
          username: 'user'
        });
        currentRepositoriesObservable.error('error message');
        fixture.detectChanges();

        currentRepositoriesObservable = mockRepositoriesObservables[currentRepositoriesObservableIndex];
        component.search({
          username: 'user'
        });
        currentRepositoriesObservable.next([]);
        fixture.detectChanges();

        expect(componentObject.errorMessage).toBeNull();
        expect(componentObject.repositoriesList).not.toBeNull();
      });
  }));
});
