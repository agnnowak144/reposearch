import { MainPageService, RepositoryApiModel, BranchApiModel } from '../main-page.service';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

interface RepositoriesResponse {
  headers: {
    get: (key: string) => string
  },
  body: RepositoryApiModel[]
}

interface BranchesResponse {
  headers: {
    get: (key: string) => string
  },
  body: BranchApiModel[]
}

const noBranchesResponse: RepositoriesResponse = {
  headers: { get: _ => null },
  body: []
};

describe('MainPageService', () => {
  let service: MainPageService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new MainPageService(httpClientSpy as any);
  });

  const isBranchesUrl = (url: string) => url.includes('branches');

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get 1 page of repositories without branches', () => {
    const response: RepositoriesResponse = {
      headers: { get: _ => null },
      body: [{
        name: 'repo',
        owner: { login: 'owner' },
        fork: false
      }]
    };

    httpClientSpy.get.and.callFake((url: string) => {
      if (isBranchesUrl(url)) {
        return of(noBranchesResponse);
      }
      return of(response);
    });

    let gotResultFromService = false;

    service.getRepositoriesForUser('test')
      .pipe(take(1))
      .subscribe(value => {
        gotResultFromService = true;

        expect(value.length).toEqual(1);
        expect(value[0].name).toEqual('repo');
        expect(value[0].owner).toEqual('owner');
        expect(value[0].branches.length).toEqual(0);
      });

    expect(httpClientSpy.get.calls.count()).toBe(2);
    expect(gotResultFromService).toBeTrue();
  });

  it('should get 1 page of repositories with a branch', () => {
    const repositoryResponse: RepositoriesResponse = {
      headers: { get: _ => null },
      body: [{
        name: 'repo',
        owner: { login: 'owner' },
        fork: false
      }]
    };

    const branchesResponse: BranchesResponse = {
      headers: { get: _ => null },
      body: [{
        name: 'branch',
        commit: {
          sha: 'sha'
        }
      }]
    };

    httpClientSpy.get.and.callFake((url: string) => {
      if (isBranchesUrl(url)) {
        return of(branchesResponse);
      }
      return of(repositoryResponse);
    });

    let gotResultFromService = false;

    service.getRepositoriesForUser('test')
      .pipe(take(1))
      .subscribe(value => {
        gotResultFromService = true;

        expect(value.length).toEqual(1);
        expect(value[0].name).toEqual('repo');
        expect(value[0].owner).toEqual('owner');
        expect(value[0].branches.length).toEqual(1);
        expect(value[0].branches[0].name).toEqual('branch');
        expect(value[0].branches[0].lastCommitSha).toEqual('sha');
      });

    expect(httpClientSpy.get.calls.count()).toBe(2);
    expect(gotResultFromService).toBeTrue();
  });

  it('should filter non-fork repositories', () => {
    const repositoriesResponse: RepositoriesResponse = {
      headers: { get: _ => null },
      body: [{
        name: 'repo 1',
        owner: { login: 'owner 1' },
        fork: false
      }, {
        name: 'repo 2',
        owner: { login: 'owner 1' },
        fork: true
      }]
    };

    httpClientSpy.get.and.callFake((url: string) => {
      if (isBranchesUrl(url)) {
        return of(noBranchesResponse);
      }
      return of(repositoriesResponse);
    });

    let gotResultFromService = false;

    service.getRepositoriesForUser('test')
      .pipe(take(1))
      .subscribe(value => {
        gotResultFromService = true;

        expect(value.length).toEqual(1);
        expect(value[0].name).toEqual('repo 1');
      });

    expect(gotResultFromService).toBeTrue();
    expect(httpClientSpy.get.calls.count()).toBe(2);
  });

  it('should support paging repositories', () => {
    const responses: RepositoriesResponse[] = [
      {
        headers: { get: _ => 'rel="next"' },
        body: [{
          name: 'repo 1',
          owner: { login: 'owner 1' },
          fork: false
        }]
      },
      {
        headers: { get: _ => 'rel="next"' },
        body: [{
          name: 'repo 2',
          owner: { login: 'owner 2' },
          fork: false
        }]
      },
      {
        headers: { get: _ => null },
        body: [{
          name: 'repo 3',
          owner: { login: 'owner 3' },
          fork: false
        }]
      }
    ];

    let pagesLoaded = 0;

    httpClientSpy.get.and.callFake((url: string) => {
      if (isBranchesUrl(url)) {
        return of(noBranchesResponse);
      }

      const result = of(responses[pagesLoaded]);
      pagesLoaded++;
      return result;
    });

    let gotResultFromService = false;

    service.getRepositoriesForUser('test')
      .pipe(take(1))
      .subscribe(value => {
        gotResultFromService = true;

        expect(value.length).toEqual(3);
        expect(value[0].name).toEqual('repo 1');
        expect(value[1].name).toEqual('repo 2');
        expect(value[2].name).toEqual('repo 3');
      });

    expect(httpClientSpy.get.calls.count()).toBe(6);
    expect(gotResultFromService).toBeTrue();
  });

  it('should support paging branches', () => {
    const repositoriesResponse: RepositoriesResponse =
    {
      headers: { get: _ => null },
      body: [{
        name: 'repo',
        owner: { login: 'owner' },
        fork: false
      }]
    }
      ;

    const branchesResponses: BranchesResponse[] = [
      {
        headers: { get: _ => 'rel="next"' },
        body: [{
          name: 'branch 1',
          commit: {
            sha: 'sha 1'
          }
        }]
      },
      {
        headers: { get: _ => 'rel="next"' },
        body: [{
          name: 'branch 2',
          commit: {
            sha: 'sha 2'
          }
        }]
      },
      {
        headers: { get: _ => null },
        body: [{
          name: 'branch 3',
          commit: {
            sha: 'sha 3'
          }
        }]
      }];

    let pagesLoaded = 0;

    httpClientSpy.get.and.callFake((url: string) => {
      if (isBranchesUrl(url)) {
        const result = of(branchesResponses[pagesLoaded]);
        pagesLoaded++;
        return result;
      }

      return of(repositoriesResponse);
    });

    let gotResultFromService = false;

    service.getRepositoriesForUser('test')
      .pipe(take(1))
      .subscribe(value => {
        gotResultFromService = true;

        expect(value.length).toEqual(1);
        expect(value[0].name).toEqual('repo');
        expect(value[0].branches.length).toEqual(3);
        expect(value[0].branches[0].name).toEqual('branch 1');
        expect(value[0].branches[1].name).toEqual('branch 2');
        expect(value[0].branches[2].name).toEqual('branch 3');
      });

    expect(httpClientSpy.get.calls.count()).toBe(4);
    expect(gotResultFromService).toBeTrue();
  });
});


