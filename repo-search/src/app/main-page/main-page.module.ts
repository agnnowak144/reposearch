import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MainPageComponent } from './main-page.component';
import { RepositoriesFilterComponent } from './repositories-filter/repositories-filter.component';
import { RepositoriesListComponent } from './repositories-list/repositories-list.component';
import { BranchesListComponent } from './repositories-list/branches-list/branches-list.component';

@NgModule({
  declarations: [
    MainPageComponent,
    RepositoriesFilterComponent,
    BranchesListComponent,
    RepositoriesListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class MainPageModule { }
