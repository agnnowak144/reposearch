import { Injectable } from '@angular/core';
import { Repository } from './shared/repository';
import { of, Observable, empty, forkJoin } from "rxjs";
import { switchMap, map, expand, last, pluck, concatMap } from "rxjs/operators";
import { HttpClient } from '@angular/common/http';
import { GITHUB_AUTHORIZATION_TOKEN } from './settings';
import { Branch } from './shared/branch';

export interface RepositoryApiModel {
  name: string;
  owner: {
    login: string;
  };
  fork: boolean;
}

export interface BranchApiModel {
  name: string;
  commit: {
    sha: string;
  };
}

interface ApiPagesIteration<T> {
  nextPage: number;
  continue: boolean;
  result: T[];
}

@Injectable({
  providedIn: 'root'
})
export class MainPageService {
  constructor(private http: HttpClient) {
  }

  getRepositoriesForUser(username: string): Observable<Repository[]> {
    return this.getRepositoriesSource(username)
      .pipe(
        concatMap(repositories =>
          forkJoin(repositories.map(repo =>
            forkJoin({
              repository: of(repo),
              branches: this.getRepositoryBranchesSource(repo.owner, repo.name)
            }).pipe(
              map(bothValues => ({ ...bothValues.repository, branches: bothValues.branches } as Repository))
            )
          ))
        ),
      );
  }

  private getRepositoriesSource(username: string): Observable<Repository[]> {
    const urlWithPage = (page: number) => `https://api.github.com/users/${username}/repos?type=all&page=${page}`;
    const mapApiResponseToViewModel = (responseBody: any) => this.mapResponseToRepositoryList(this.filterNonForksFromResponse(responseBody as RepositoryApiModel[]));
    return this.getDataSource<Repository>(urlWithPage, mapApiResponseToViewModel);
  }

  private filterNonForksFromResponse = (response: RepositoryApiModel[]) => response.filter(item => !item.fork);

  private mapResponseToRepositoryList = (response: RepositoryApiModel[]): Repository[] => {
    return response.map(item => ({
      name: item.name,
      owner: item.owner.login,
      branches: []
    }));
  }

  private getRepositoryBranchesSource(username: string, repository: string): Observable<Branch[]> {
    const urlWithPage = (page: number) => `https://api.github.com/repos/${username}/${repository}/branches?page=${page}`;
    const mapApiResponseToViewModel = (responseBody: any) => this.mapResponseToBranchList(responseBody as BranchApiModel[]);
    return this.getDataSource<Branch>(urlWithPage, mapApiResponseToViewModel);
  }

  private mapResponseToBranchList = (response: BranchApiModel[]): Branch[] => {
    return response.map(item => ({
      name: item.name,
      lastCommitSha: item.commit.sha
    }));
  }

  private getDataSource<TResult>(sourceUrlWithPage: (page: number) => string, mapApiResponseToViewModel: (responseBody: any) => TResult[]): Observable<TResult[]> {
    return of(
      {
        nextPage: 1,
        continue: true,
        result: []
      } as ApiPagesIteration<TResult>)
      .pipe(
        expand(value => {
          if (!value.continue) {
            return empty();
          }

          const requestHeaders = !!GITHUB_AUTHORIZATION_TOKEN ? { Authorization: `token ${GITHUB_AUTHORIZATION_TOKEN}` } : null;

          return this.http.get(sourceUrlWithPage(value.nextPage), { observe: 'response', headers: requestHeaders })
            .pipe(
              map(r => {
                const linkHeaderValue = r.headers.get("Link");
                const hasNextPattern = /rel="next"/;
                const hasNext = !!linkHeaderValue && !!linkHeaderValue.match(hasNextPattern);
                return { ...r, hasNextPage: hasNext };
              })
            )
            .pipe(
              switchMap(ajaxResult => {
                let nextValue = { ...value };
                nextValue.nextPage++;
                nextValue.continue = ajaxResult.hasNextPage;
                const currentPageResult = mapApiResponseToViewModel(ajaxResult.body);
                nextValue.result = value.result.concat(currentPageResult as TResult[]);
                return of(nextValue);
              })
            );
        }),
        pluck("result"),
        last()
      );
  }
}
