import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageModule } from './main-page/main-page.module';
import { MainPageComponent } from './main-page/main-page.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MainPageModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
